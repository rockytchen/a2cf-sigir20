from __future__ import print_function

import tensorflow as tf
import random
import numpy
import glob
import sklearn
from numpy import genfromtxt
from numpy import array
from sklearn import metrics
import math

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

##############
#data handler#
##############
class DataHandler(object):

    def __init__(self, file_path_test, file_path_neg):
        '''
        initialize all data
        '''
        self.data_interaction = []
        '''
        new feature format:
        [ 
        line 0, 1, 2: [query user (int), query item (int), real item (int)]
        line 3 ~ -1: [negtive poi (int)]
        ]
        '''
        print("-> loading interaction data now...")
        with open(file_path_test) as in_file:
            all_trans_dic = {}
            for each_line_raw in in_file:
                each_line = each_line_raw.rstrip().split(",")[:-1]
                user_index = int(each_line[0])
                item_query_index = int(each_line[1])
                item_real_index = int(each_line[2]) # query user, query item, real item
                if user_index not in all_trans_dic:
                    all_trans_dic[user_index] = [[item_query_index, item_real_index]]
                else:
                    all_trans_dic[user_index].append([item_query_index, item_real_index])
            
            with open(file_path_neg) as in_file:
                for each_line_raw in in_file:
                    each_line = each_line_raw.rstrip().split(",")[:-1]
                    user_index = int(each_line[0])
                    if user_index in all_trans_dic:
                        for each_pair in all_trans_dic[user_index]:
                            self.data_interaction.append([user_index, each_pair[0], each_pair[1]] + [int(x) for x in each_line[1:]])

        random.shuffle(self.data_interaction)    
        self.sample_num = len(self.data_interaction)
        self.neg_num = len(self.data_interaction[0][3:])
        self.batch_id = 0
        print("-> all data loaded.")

    def next(self):
        '''
        Return one test sample at a time.
        '''
        if self.batch_id == len(self.data_interaction):
            self.batch_id = 0

        batch_feature = self.data_interaction[self.batch_id]
        batch_first_section = [[batch_feature[0]] for x in range(self.neg_num + 1)]
        batch_second_section = [[batch_feature[1]] for x in range(self.neg_num + 1)]
        batch_third_section = [[x] for x in batch_feature[2:]] #1 + 1000 test cases

        self.batch_id = min(self.batch_id + 1, len(self.data_interaction))
        return batch_first_section, batch_second_section, batch_third_section

'''
network parameters
'''
user_dim = 515813
item_dim = 58534
attribute_dim = 2801

neg_size = 1000

N = 5 # maximum score
batch_size = 1
hidden_dim = 64
beta = 8
epsilon = 8
gamma = 0.7 # 0<gamma<1

dropout_keep_prob_test = 1.0

'''
load and split data
'''
data_handler = DataHandler("data/test_interaction_feature.csv", "data/negative_sample.csv")
print("-> loading the tf graph now...")

'''
running parameters
'''
display_step = 100
max_step = 100000

'''
tf graph input
'''
u_query = tf.placeholder("int32", [None, 1]) #(batch, 1)
v_query = tf.placeholder("int32", [None, 1]) #(batch, 1)
v_candidate = tf.placeholder("int32", [None, 1]) #(batch, 1)
u_UACF = tf.placeholder("int32", [None, 1]) #(batch, 1)
a_UACF = tf.placeholder("int32", [None, 1]) #(batch, 1)
p_real = tf.placeholder("float", [None, 1]) #(batch, 1)
v_IACF = tf.placeholder("int32", [None, 1]) #(batch, 1)
a_IACF = tf.placeholder("int32", [None, 1]) #(batch, 1)
q_real = tf.placeholder("float", [None, 1]) #(batch, 1)

dropout_keep_prob = tf.placeholder("float")

'''
define parameters (weights & biases)
'''
params = {
    #____ embedding weights ____
    "user_embedding": tf.get_variable("user_embedding", shape = [user_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "item_embedding": tf.get_variable("item_embedding", shape = [item_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "attribute_embedding": tf.get_variable("helpful_embedding", shape = [attribute_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    #____ FM weights ____    
    "s_projection_weight": tf.get_variable("s_projection_weight", shape = [2*hidden_dim, 1], trainable = True, initializer = tf.glorot_uniform_initializer),
    "p_projection_weight": tf.get_variable("p_projection_weight", shape = [2*hidden_dim, 1], trainable = True, initializer = tf.glorot_uniform_initializer)}

#with tf.variable_scope("PnQ", reuse = tf.AUTO_REUSE):
with tf.device("/CPU:0"):    
    params["P"] = tf.get_variable("P", trainable = False, initializer = tf.zeros([user_dim, attribute_dim]), caching_device = "/CPU:0")
    params["Q"] = tf.get_variable("Q", trainable = False, initializer = tf.zeros([item_dim, attribute_dim]), caching_device = "/CPU:0")

def EstPnQ(params):
    '''
    The NCF approach is too slow when estimating the full matrices, hence we replace that with a quicker multiplication form.
    With well trained user/item/attribute embeddings, we find that there are no obvious differences in the actual recommendation performance. 
    '''
    with tf.device("/CPU:0"):
        P = tf.nn.tanh(tf.matmul(params["user_embedding"], tf.transpose(params["attribute_embedding"])))
        P = (0.5*N - 0.5)*P +  0.5*N + 0.5
        Q = tf.nn.tanh(tf.matmul(params["item_embedding"], tf.transpose(params["attribute_embedding"])))
        Q = (0.5*N - 0.5)*Q +  0.5*N + 0.5
        
        assign_op_P = tf.assign(params["P"], P)
        assign_op_Q = tf.assign(params["Q"], Q)
    return assign_op_P, assign_op_Q

def A2CF(params, u_query, v_query, v_candidate): #input format: 1*(u_query, v_query, v+) + 8*(u_query, v_query, v-)
    '''
    note that due to negative sampling, the batch size here is now (1+neg_size)*orginal_batch_size
    embedding layer
    '''
    u_query_emb = tf.reshape(tf.nn.embedding_lookup(params["user_embedding"], u_query), [-1, hidden_dim]) 
    v_query_emb = tf.reshape(tf.nn.embedding_lookup(params["item_embedding"], v_query), [-1, hidden_dim])
    v_candidate_emb = tf.reshape(tf.nn.embedding_lookup(params["item_embedding"], v_candidate), [-1, hidden_dim]) #(batch, hidden)

    '''
    fetch corresponding lines from P and Q
    '''
    p_query = tf.reshape(tf.nn.embedding_lookup(params["P"], u_query), [-1, attribute_dim])
    q_query = tf.reshape(tf.nn.embedding_lookup(params["Q"], v_query), [-1, attribute_dim])
    q_candidate = tf.reshape(tf.nn.embedding_lookup(params["Q"], v_candidate), [-1, attribute_dim]) #(batch, attr_size)
    
    '''
    calculate substitution affinity
    '''
    phi = tf.nn.softmax(tf.multiply(q_query, q_candidate)/beta) #(batch, attr_size)
    v_candidate_tilde_1 = tf.matmul(phi, params["attribute_embedding"]) #(batch, hidden)
    f_s_temp = tf.concat([tf.multiply(v_query_emb, v_candidate_emb), v_candidate_tilde_1], 1) #(batch, 2*hidden)
    f_s = tf.matmul(f_s_temp, params["s_projection_weight"]) #(batch, 1)

    '''
    calculate personalization affinity
    '''
    ita = tf.nn.softmax(tf.multiply(p_query, q_candidate)/epsilon) #(batch, attr_size)
    v_candidate_tilde_2 = tf.matmul(ita, params["attribute_embedding"]) #(batch, hidden)
    f_p_temp = tf.concat([tf.multiply(u_query_emb, v_candidate_emb), v_candidate_tilde_2], 1) #(batch, 2*hidden)
    f_p = tf.matmul(f_p_temp, params["p_projection_weight"]) #(batch, 1)

    '''
    BPR-S loss function
    '''
    f_rank = f_s*gamma + f_p*(1-gamma) #(batch, 1)
    f_rank = tf.reshape(f_rank, [-1, 1+neg_size]) #(batch(og), 1+neg_size)
    f_rank_pos, f_rank_neg = tf.split(f_rank, [1, neg_size], 1) #(batch(og), 1) and (batch(og), neg_size)

    return f_rank_pos, f_rank_neg

'''
get the predictions
'''
new_P, new_Q = EstPnQ(params)
f_pos, f_neg = A2CF(params, u_query, v_query, v_candidate)

print("-> tf graph loaded.")
print("-> start testing now...")

'''
start testing with recovered model parameters
'''
saver = tf.train.Saver()
with tf.Session() as sess:
    saver.restore(sess, "saved_params/A2CF/stage_4.ckpt")
    print("-> model restored.")
    sess.run([new_P, new_Q]) #precompute P and Q
    # Keep training until reach max steps
    all_hits = {1:0, 5:0.0, 10:0.0, 20:0.0, 50:0.0}
    all_mrr = {1:0, 5:0.0, 10:0.0, 20:0.0, 50:0.0}
    all_ndcg = {1:0, 5:0.0, 10:0.0, 20:0.0, 50:0.0}

    while data_handler.batch_id < data_handler.sample_num:
        batch_u_query, batch_v_query, batch_v_candidate = data_handler.next()

        score_pos_og, score_neg = sess.run([f_pos, f_neg], feed_dict = {u_query: batch_u_query,
                                                                        v_query: batch_v_query,
                                                                    v_candidate: batch_v_candidate})

        score_pos = score_pos_og[0,0]
        score_neg = numpy.fliplr(numpy.sort(score_neg))

        if score_pos >= score_neg[0,49]:
            all_hits[50] += 1.0
            if score_pos >= score_neg[0,19]:
                all_hits[20] += 1.0
                if score_pos >= score_neg[0,9]:
                    all_hits[10] += 1.0
                    if score_pos >= score_neg[0,4]:
                        all_hits[5] += 1.0
                        if score_pos >= score_neg[0,0]:
                            all_hits[1] += 1.0
        
        for i in range(50):
            if score_pos >= score_neg[0,i]:
                mrr = 1.0/(1 + float(i+1))
                score_pos = -10000.0
                if i <= 49:
                    all_mrr[50] += mrr
                    if i <= 19:
                        all_mrr[20] += mrr
                        if i <= 9:
                            all_mrr[10] += mrr
                            if i <= 4:
                                all_mrr[5] += mrr
                                if i == 1:
                                    all_mrr[1] += mrr

        score_pos = score_pos_og[0,0]
        for i in range(50):
            if score_pos >= score_neg[0,i]:
                ndcg = 1.0/math.log((1 + float(i+1)), 2.0)
                score_pos = -10000.0
                if i <= 49:
                    all_ndcg[50] += ndcg
                    if i <= 19:
                        all_ndcg[20] += ndcg
                        if i <= 9:
                            all_ndcg[10] += ndcg
                            if i <= 4:
                                all_ndcg[5] += ndcg
                                if i == 1:
                                    all_ndcg[1] += ndcg
                
        if data_handler.batch_id % 100 == 0:
            hit1_avg = all_hits[1]/float(data_handler.batch_id)
            hit5_avg = all_hits[5]/float(data_handler.batch_id)
            hit10_avg = all_hits[10]/float(data_handler.batch_id)
            hit20_avg = all_hits[20]/float(data_handler.batch_id)
            hit50_avg = all_hits[50]/float(data_handler.batch_id)

            mrr1_avg = all_mrr[1]/float(data_handler.batch_id)
            mrr5_avg = all_mrr[5]/float(data_handler.batch_id)
            mrr10_avg = all_mrr[10]/float(data_handler.batch_id)
            mrr20_avg = all_mrr[20]/float(data_handler.batch_id)
            mrr50_avg = all_mrr[50]/float(data_handler.batch_id)

            ndcg1_avg = all_ndcg[1]/float(data_handler.batch_id)
            ndcg5_avg = all_ndcg[5]/float(data_handler.batch_id)
            ndcg10_avg = all_ndcg[10]/float(data_handler.batch_id)
            ndcg20_avg = all_ndcg[20]/float(data_handler.batch_id)
            ndcg50_avg = all_ndcg[50]/float(data_handler.batch_id)
            print("-> tested samples: " + str(data_handler.batch_id) + ",\n" +
                  " hit@5, 10, 20, 50: " + "{:.4f}".format(hit5_avg) + "," + "{:.4f}".format(hit10_avg) + "," + "{:.4f}".format(hit20_avg) + "," + "{:.4f}".format(hit50_avg) + ",\n" +
                  " mrr@5, 10, 20, 50: " + "{:.4f}".format(mrr5_avg) + "," + "{:.4f}".format(mrr10_avg) + "," + "{:.4f}".format(mrr20_avg) + "," + "{:.4f}".format(mrr50_avg) + ",\n" +
                  "ndcg@5, 10, 20, 50: " + "{:.4f}".format(ndcg5_avg) + "," + "{:.4f}".format(ndcg10_avg) + "," + "{:.4f}".format(ndcg20_avg) + "," + "{:.4f}".format(ndcg50_avg))

    hit1_avg = all_hits[1]/float(data_handler.sample_num)
    hit5_avg = all_hits[5]/float(data_handler.sample_num)
    hit10_avg = all_hits[10]/float(data_handler.sample_num)
    hit20_avg = all_hits[20]/float(data_handler.sample_num)
    hit50_avg = all_hits[50]/float(data_handler.sample_num)

    mrr1_avg = all_mrr[1]/float(data_handler.sample_num)
    mrr5_avg = all_mrr[5]/float(data_handler.sample_num)
    mrr10_avg = all_mrr[10]/float(data_handler.sample_num)
    mrr20_avg = all_mrr[20]/float(data_handler.sample_num)
    mrr50_avg = all_mrr[50]/float(data_handler.sample_num)

    ndcg1_avg = all_ndcg[1]/float(data_handler.sample_num)
    ndcg5_avg = all_ndcg[5]/float(data_handler.sample_num)
    ndcg10_avg = all_ndcg[10]/float(data_handler.sample_num)
    ndcg20_avg = all_ndcg[20]/float(data_handler.sample_num)
    ndcg50_avg = all_ndcg[50]/float(data_handler.sample_num)
    print("\n-> test finished" + ",\n" +
          " hit@5, 10, 20, 50: " + "{:.4f}".format(hit5_avg) + "," + "{:.4f}".format(hit10_avg) + "," + "{:.4f}".format(hit20_avg) + "," + "{:.4f}".format(hit50_avg) + ",\n" +
          " mrr@5, 10, 20, 50: " + "{:.4f}".format(mrr5_avg) + "," + "{:.4f}".format(mrr10_avg) + "," + "{:.4f}".format(mrr20_avg) + "," + "{:.4f}".format(mrr50_avg) + ",\n" +
          "ndcg@5, 10, 20, 50: " + "{:.4f}".format(ndcg5_avg) + "," + "{:.4f}".format(ndcg10_avg) + "," + "{:.4f}".format(ndcg20_avg) + "," + "{:.4f}".format(ndcg50_avg))


    print("-> test finished.")