from __future__ import print_function

import tensorflow as tf
import random
import numpy
import glob
import sklearn
from numpy import genfromtxt
from numpy import array
from sklearn import metrics
import math

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ['CUDA_VISIBLE_DEVICES'] = '2'

##############
#data handler#
##############
class DataHandler(object):

    def __init__(self, file_path_interaction, file_path_UACF, file_path_IACF):
        '''
        initialize all data
        '''
        self.data_interaction = []
        self.data_UACF = []
        self.data_IACF = []
        self.neg_num = 0
        
        print("-> loading interaction data now...")
        with open(file_path_interaction) as in_file:
            for each_line_raw in in_file:
                each_line = each_line_raw.rstrip().split(",")[:-1]
                feature_line = [int(x) for x in each_line]
                '''
                feature format: 
                [user (int), item_query (int), item_real (int), item_neg (int) x n]
                '''
                self.data_interaction.append(feature_line)
        random.shuffle(self.data_interaction)
        self.neg_num = len(self.data_interaction[0][3:])
        
        print("-> loading UACF data now...")
        with open(file_path_UACF) as in_file:
            for each_line_raw in in_file:
                each_line = each_line_raw.rstrip().split(",")[:-1]
                feature_line = [int(x) for x in each_line[:-1]]
                feature_line.append(float(each_line[-1]))
                '''
                feature format:
                [user (int), attribute (int), score (float)]
                '''
                self.data_UACF.append(feature_line)
        random.shuffle(self.data_UACF)

        print("-> loading IACF data now...")
        with open(file_path_IACF) as in_file:
            for each_line_raw in in_file:
                each_line = each_line_raw.rstrip().split(",")[:-1]
                feature_line = [int(x) for x in each_line[:-1]]
                feature_line.append(float(each_line[-1]))
                '''
                feature format:
                [item (int), attribute (int), score (float)]
                '''
                self.data_IACF.append(feature_line)
        random.shuffle(self.data_IACF)

        self.batch_id = {"A2CF": 0, "UACF": 0, "IACF": 0}
        self.epoch_train = {"A2CF": 0, "UACF": 0, "IACF": 0}
        print("-> all data loaded.")

    def next(self, mode, batch_size):
        '''
        Return a batch of data. When dataset end is reached, start over.
        '''
        data_list = []
        if mode == "A2CF":
            data_list = self.data_interaction
        elif mode == "UACF":
            data_list = self.data_UACF
        elif mode == "IACF":
            data_list = self.data_IACF
        else:
            print("-> wrong mode flag used. use A2CF, UACF, or IACF.")

        if self.batch_id[mode] == len(data_list):
            self.batch_id[mode] = 0
            self.epoch_train[mode] += 1

        batch_feature = data_list[self.batch_id[mode]:min(self.batch_id[mode] + batch_size, len(data_list))]
        batch_first_section = []
        batch_second_section = []
        batch_third_section = []

        if mode == "UACF" or mode == "IACF":
            batch_first_section = [[line[0]] for line in batch_feature] # user (int) or item (int) in any case
            batch_second_section = [[line[1]] for line in batch_feature] # attribute (int)
            batch_third_section = [[x for x in line[2:]] for line in batch_feature] # score (float)
        else:
            for each_line in batch_feature:
                batch_first_section = batch_first_section + [[each_line[0]] for x in range(self.neg_num + 1)] # user (int)
                batch_second_section = batch_second_section + [[each_line[1]] for x in range(self.neg_num + 1)] # query item (int)
                batch_third_section = batch_third_section + [[x] for x in each_line[2:]] # real item (int) + neg items (int)

        self.batch_id[mode] = min(self.batch_id[mode] + batch_size, len(data_list))
        return batch_first_section, batch_second_section, batch_third_section

'''
network parameters
'''
user_dim = 515813
item_dim = 58534
attribute_dim = 2801

neg_size = 2
neg_size = 4*neg_size

N = 5 # maximum score
batch_size = 512
hidden_dim = 64
beta = 8
epsilon = 8
gamma = 0.7 # 0<gamma<1

dropout_keep_prob_train = 0.6
step_size = 0.001

T_epoch1 = 1
T_epoch2 = 1

'''
load and split data
'''
data_handler = DataHandler("data/train_interaction_feature.csv", "data/UACF_input.csv", "data/IACF_input.csv")

print("-> loading the tf graph now...")

'''
running parameters
'''
display_step = 100
max_step = 10

'''
tf graph input
'''
u_query = tf.placeholder("int32", [None, 1]) #(batch, 1)
v_query = tf.placeholder("int32", [None, 1]) #(batch, 1)
v_candidate = tf.placeholder("int32", [None, 1]) #(batch, 1)
u_UACF = tf.placeholder("int32", [None, 1]) #(batch, 1)
a_UACF = tf.placeholder("int32", [None, 1]) #(batch, 1)
p_real = tf.placeholder("float", [None, 1]) #(batch, 1)
v_IACF = tf.placeholder("int32", [None, 1]) #(batch, 1)
a_IACF = tf.placeholder("int32", [None, 1]) #(batch, 1)
q_real = tf.placeholder("float", [None, 1]) #(batch, 1)

dropout_keep_prob = tf.placeholder("float")

'''
define parameters (weights & biases)
'''
params = {
    #____ embedding weights ____
    "user_embedding": tf.get_variable("user_embedding", shape = [user_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "item_embedding": tf.get_variable("item_embedding", shape = [item_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "attribute_embedding": tf.get_variable("helpful_embedding", shape = [attribute_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    #____ FM weights ____    
    "s_projection_weight": tf.get_variable("s_projection_weight", shape = [2*hidden_dim, 1], trainable = True, initializer = tf.glorot_uniform_initializer),
    "p_projection_weight": tf.get_variable("p_projection_weight", shape = [2*hidden_dim, 1], trainable = True, initializer = tf.glorot_uniform_initializer)}

#with tf.variable_scope("PnQ", reuse = tf.AUTO_REUSE):
with tf.device("/CPU:0"):    
    params["P"] = tf.get_variable("P", trainable = False, initializer = tf.zeros([user_dim, attribute_dim]), caching_device = "/CPU:0")
    params["Q"] = tf.get_variable("Q", trainable = False, initializer = tf.zeros([item_dim, attribute_dim]), caching_device = "/CPU:0")
    

####################
####### A2CF #######
####################
def UACF(params, u_UACF, a_UACF, p_real, dropout_keep_prob):
    '''
    embedding layer
    '''
    u_emb = tf.nn.embedding_lookup(params["user_embedding"], u_UACF) #(batch, 1, hidden)
    u_emb = tf.reshape(u_emb, [-1, hidden_dim]) #(batch, hidden)
    if a_UACF != None:
        a_emb = tf.nn.embedding_lookup(params["attribute_embedding"], a_UACF) #(batch, 1, hidden)
        a_emb = tf.reshape(a_emb, [-1, hidden_dim]) #(batch, hidden)
    else:
        a_emb = params["attribute_embedding"]

    '''
    dropout for embedding layer
    '''
    u_emb = tf.nn.dropout(u_emb, dropout_keep_prob)
    a_emb = tf.nn.dropout(a_emb, dropout_keep_prob)

    '''
    residual FFN layer
    '''
    h_0 = tf.concat([u_emb, a_emb], 1) # (batch, 2*hidden)
    h_1 = tf.layers.dense(tf.contrib.layers.layer_norm(h_0, begin_norm_axis = 1, begin_params_axis = -1), units = 2*hidden_dim, activation = tf.nn.relu, use_bias=True) #(batch, hidden)    
    h_1 = tf.add(h_0, tf.nn.dropout(h_1, dropout_keep_prob)) #(batch, hidden)

    '''
    prediction layer
    '''
    p_pred = tf.layers.dense(tf.contrib.layers.layer_norm(h_1, begin_norm_axis = 1, begin_params_axis = -1), units = 1, activation = tf.nn.tanh, use_bias=True) #(batch, 1)
    p_pred = (0.5*N - 0.5)*p_pred + 0.5*N + 0.5 #rescaled tanh

    '''
    summarizing batch loss
    '''
    this_batch_size = tf.cast(tf.shape(p_pred)[0], tf.float32)
    if p_real != None:
        loss = tf.reduce_sum(tf.square(tf.subtract(p_real, p_pred)))/this_batch_size
        loss = loss*batch_size
    else:
        loss = None

    return p_pred, loss

def IACF(params, v_IACF, a_IACF, q_real, dropout_keep_prob):
    '''
    embedding layer
    '''
    v_emb = tf.nn.embedding_lookup(params["item_embedding"], v_IACF)
    v_emb = tf.reshape(v_emb, [-1, hidden_dim]) #(batch, hidden)
    if a_IACF != None:
        a_emb = tf.nn.embedding_lookup(params["attribute_embedding"], a_IACF)
        a_emb = tf.reshape(a_emb, [-1, hidden_dim]) #(batch, hidden)
    else:
        a_emb = params["attribute_embedding"]
    
    '''
    dropout for embedding layer
    '''
    v_emb = tf.nn.dropout(v_emb, dropout_keep_prob)
    a_emb = tf.nn.dropout(a_emb, dropout_keep_prob)

    '''
    residual FFN layer
    '''
    h_0 = tf.concat([v_emb, a_emb], 1) # (batch, 2*hidden)
    h_1 = tf.layers.dense(tf.contrib.layers.layer_norm(h_0, begin_norm_axis = 1, begin_params_axis = -1), units = 2*hidden_dim, activation = tf.nn.relu, use_bias=True) #(batch, hidden)    
    h_1 = tf.add(h_0, tf.nn.dropout(h_1, dropout_keep_prob)) #(batch, hidden)

    '''
    prediction layer
    '''
    q_pred = tf.layers.dense(tf.contrib.layers.layer_norm(h_1, begin_norm_axis = 1, begin_params_axis = -1), units = 1, activation = tf.nn.tanh, use_bias=True) #(batch, 1)
    q_pred = (0.5*N - 0.5)*q_pred + 0.5*N + 0.5 #rescaled tanh

    '''
    summarizing batch loss
    '''
    this_batch_size = tf.cast(tf.shape(q_pred)[0], tf.float32)
    if q_real != None:
        loss = tf.reduce_sum(tf.square(tf.subtract(q_real, q_pred)))/this_batch_size
        loss = loss*batch_size
    else:
        loss = None

    return q_pred, loss

def EstPnQ(params):
    '''
    The NCF approach is too slow when estimating the full matrices, hence we replace that with a quicker multiplication form.
    With well trained user/item/attribute embeddings, we find that there are no obvious differences in the actual recommendation performance. 
    '''
    with tf.device("/CPU:0"):
        P = tf.nn.tanh(tf.matmul(params["user_embedding"], tf.transpose(params["attribute_embedding"])))
        P = (0.5*N - 0.5)*P + 0.5*N + 0.5
        Q = tf.nn.tanh(tf.matmul(params["item_embedding"], tf.transpose(params["attribute_embedding"])))
        Q = (0.5*N - 0.5)*Q + 0.5*N + 0.5
        
        assign_op_P = tf.assign(params["P"], P)
        assign_op_Q = tf.assign(params["Q"], Q)
    return assign_op_P, assign_op_Q

def A2CF(params, u_query, v_query, v_candidate): #input format: 1*(u_query, v_query, v+) + 8*(u_query, v_query, v-)
    '''
    note that due to negative sampling, the batch size here is now (1+neg_size)*orginal_batch_size
    embedding layer
    '''
    u_query_emb = tf.reshape(tf.nn.embedding_lookup(params["user_embedding"], u_query), [-1, hidden_dim]) 
    v_query_emb = tf.reshape(tf.nn.embedding_lookup(params["item_embedding"], v_query), [-1, hidden_dim])
    v_candidate_emb = tf.reshape(tf.nn.embedding_lookup(params["item_embedding"], v_candidate), [-1, hidden_dim]) #(batch, hidden)

    '''
    fetch corresponding lines from P and Q
    '''
    p_query = tf.reshape(tf.nn.embedding_lookup(params["P"], u_query), [-1, attribute_dim])
    q_query = tf.reshape(tf.nn.embedding_lookup(params["Q"], v_query), [-1, attribute_dim])
    q_candidate = tf.reshape(tf.nn.embedding_lookup(params["Q"], v_candidate), [-1, attribute_dim]) #(batch, attr_size)
    
    '''
    calculate substitution affinity
    '''
    phi = tf.nn.softmax(tf.multiply(q_query, q_candidate)/beta) #(batch, attr_size)
    v_candidate_tilde_1 = tf.matmul(phi, params["attribute_embedding"]) #(batch, hidden)
    f_s_temp = tf.concat([tf.multiply(v_query_emb, v_candidate_emb), v_candidate_tilde_1], 1) #(batch, 2*hidden)
    f_s = tf.matmul(f_s_temp, params["s_projection_weight"]) #(batch, 1)

    '''
    calculate personalization affinity
    '''
    ita = tf.nn.softmax(tf.multiply(p_query, q_candidate)/epsilon) #(batch, attr_size)
    v_candidate_tilde_2 = tf.matmul(ita, params["attribute_embedding"]) #(batch, hidden)
    f_p_temp = tf.concat([tf.multiply(u_query_emb, v_candidate_emb), v_candidate_tilde_2], 1) #(batch, 2*hidden)
    f_p = tf.matmul(f_p_temp, params["p_projection_weight"]) #(batch, 1)

    '''
    BPR-S loss function
    '''
    f_rank = f_s*gamma + f_p*(1-gamma) #(batch, 1)
    f_rank = tf.reshape(f_rank, [-1, 1+neg_size]) #(batch(og), 1+neg_size)
    f_rank_pos, f_rank_neg = tf.split(f_rank, [1, neg_size], 1) #(batch(og), 1) and (batch(og), neg_size)

    this_batch_size = tf.cast(tf.shape(f_rank_pos)[0], tf.float32)
    BPR_loss = tf.sigmoid(tf.subtract(tf.tile(f_rank_pos, [1, neg_size]), f_rank_neg))
    BPR_loss = 0.0 - tf.reduce_sum(tf.log(BPR_loss + 1e-24))/tf.cast(tf.shape(f_rank)[0], tf.float32)
    BPR_loss = (BPR_loss/this_batch_size)*batch_size*neg_size

    return f_rank_pos, f_rank_neg, BPR_loss

'''
get the predictions
'''
p_pred, p_loss = UACF(params, u_UACF, a_UACF, p_real, dropout_keep_prob)
q_pred, q_loss = IACF(params, v_IACF, a_IACF, q_real, dropout_keep_prob)
new_P, new_Q = EstPnQ(params)
f_pos, f_neg, BPR_loss = A2CF(params, u_query, v_query, v_candidate)

'''
optimizer
'''
train_op1 = tf.train.AdamOptimizer(learning_rate = step_size).minimize(p_loss + q_loss)
train_op2 = tf.train.AdamOptimizer(learning_rate = step_size).minimize(BPR_loss)

#optimizer = tf.train.AdamOptimizer(learning_rate = step_size).minimize(squared_loss)

'''
optimizer = tf.train.AdamOptimizer(learning_rate = step_size)
gvs = optimizer.compute_gradients(squared_loss)
capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in gvs]
train_op = optimizer.apply_gradients(capped_gvs)
'''

print("-> tf graph loaded.")
print("-> start training now...")

# Launch the traning session
saver = tf.train.Saver()
with tf.Session() as sess:
    #initialize all parameters before the training loop starts
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())
    stage = 1
    step = 1
    # Keep training until reach max steps
    best_rec_loss = 1000.0
    best_rec_epoch = 1
    
    while stage <= max_step:
        while min(data_handler.epoch_train["UACF"], data_handler.epoch_train["IACF"]) < stage*T_epoch1:
            batch_u_UACF, batch_a_UACF, batch_label_UACF = data_handler.next("UACF", batch_size)
            batch_v_IACF, batch_a_IACF, batch_label_IACF = data_handler.next("IACF", batch_size)
            #print(len(batch_v_IACF))
            #print(len(batch_a_IACF))
            #print(len(batch_label_IACF))
            
            sess.run(train_op1, feed_dict = {u_UACF: batch_u_UACF,
                                             a_UACF: batch_a_UACF, 
                                             p_real: batch_label_UACF,
                                             v_IACF: batch_v_IACF,
                                             a_IACF: batch_a_IACF, 
                                             q_real: batch_label_IACF,
                                  dropout_keep_prob: dropout_keep_prob_train})
            
            '''
            visualize training error
            '''
            if step % display_step == 0:
                p_loss_vis, q_loss_vis = sess.run([p_loss, q_loss], feed_dict = {u_UACF: batch_u_UACF,
                                                                                 a_UACF: batch_a_UACF, 
                                                                                 p_real: batch_label_UACF,
                                                                                 v_IACF: batch_v_IACF,
                                                                                 a_IACF: batch_a_IACF, 
                                                                                 q_real: batch_label_IACF,
                                                                      dropout_keep_prob: dropout_keep_prob_train})

                print("-> (UACF + IACF) stage: " + str(stage) +
                      ", UACF epoch: " + str(int(data_handler.epoch_train["UACF"]) + 1) +
                      ", IACF epoch: " + str(int(data_handler.epoch_train["IACF"]) + 1) +
                      ", learning rate: " + str(step_size) +
                      "\n   combined loss: " + "{:.3f}".format(p_loss_vis + q_loss_vis) +
                      ", UACF loss: " + "{:.3f}".format(p_loss_vis) +
                      ", IACF loss: " + "{:.3f}".format(q_loss_vis))
            step += 1

        #sess.run() #important
        print("--> estimating X and Y...")
        sess.run([new_P, new_Q])
        print(params["P"].eval())
        while data_handler.epoch_train["A2CF"] < stage*T_epoch2:
            batch_u_query, batch_v_query, batch_v_candidate = data_handler.next("A2CF", batch_size) #u_query, v_query, v_candidate

            sess.run(train_op2, feed_dict = {u_query: batch_u_query,
                                             v_query: batch_v_query,
                                         v_candidate: batch_v_candidate})
            
            '''
            visualize training error
            '''
            if step % display_step == 0:
                rec_loss_vis = sess.run(BPR_loss, feed_dict = {u_query: batch_u_query,
                                                               v_query: batch_v_query,
                                                           v_candidate: batch_v_candidate})             

                print("-> (A2CF) stage: " + str(stage) +
                      ", A2CF epoch: " + str(int(data_handler.epoch_train["A2CF"]) + 1) +
                      ", learning rate: " + str(step_size) +
                      "\n   recommendation loss: " + "{:.3f}".format(rec_loss_vis))

                best_rec_loss = min(best_rec_loss, rec_loss_vis)
                if best_rec_loss >= rec_loss_vis:
                    best_rec_loss = rec_loss_vis
                    best_rec_epoch = data_handler.epoch_train["A2CF"] + 1
            
                print("   best recommendation loss: " + "{:.3f}".format(best_rec_loss) + 
                      ", observed at epoch: " + str(best_rec_epoch))
            step += 1

        save_path = saver.save(sess, "saved_params/A2CF/stage_" + str(stage) + ".ckpt")
        print("-> model saved for stage " + str(stage) + ".")
    
        stage += 1
    print("-> trainig finished.")