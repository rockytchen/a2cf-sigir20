# README #

This is the README file for the Tensorflow implementation of A2CF.

### HOWTO ###

(1) The model is implemented and tested with Tensorflow-GPU 1.12.0 and Numpy 1.14.0. We also have NVIDIA-SMI v430.26 and CUDA v10.0 installed.

(2) To test the model, simply run "02_A2CF_xxx_Eval.py". We provide pre-trained parameters in the "saved_params" folder. 
	A sample dataset is available at http://bit.ly/sample-Office.

(3) To re-run the training process with your own data/parameters/tasks, modify the model details in both "2_A2CF_xxx.py" and "02_A2CF_xxx_Eval.py", and then run "2_A2CF_xxx.py" to train the model from scratch. 
	New parameters will be saved in the "saved_params" folder.
	To test for your own data/parameters/tasks, simply repeat step (2) with your specified .ckpt file.
	
Note: pay attention to common errors like data format, file path etc. during your modification.

### BIBTEX ###

If you find our work helpful, please cite our paper:

@article{chen2020try,
  title={Try This Instead: Personalized and Interpretable Substitute Recommendation},
  author={Chen, Tong and Yin, Hongzhi and Ye, Guanhua and Huang, Zi and Wang, Yang and Wang, Meng},
  journal={SIGIR},
  year={2020}
}